import { useLocation } from "react-router-dom";
import "./App.css";

import { UserContext } from "./utils/contextProviders/UserContext";
import { useEffect, useState } from "react";
import MainComponent from "./components/subComponents/MainComponent";
// import ThemeProvider from "./components/subComponents/mainComponent/ThemeProvider";
// import AlertMessage from "./components/utilComponents/AlertMessage";
// import { message } from "antd";
// import { setSessionItem } from "./utils/helperFunctions/sessionStorageHelper";

function App() {
  const [user, setUser] = useState({ token: null });
  // const [theme, setTheme] = useState({ mode: "dark", currentMenu: "" });
  // const [messageApi, contextHolder] = message.useMessage();
  const locationHook = useLocation();
  // const [loadingState, setLoadingState] = useState(false);

  useEffect(() => {
    // const userData = sessionStorage.getItem("userData");
    // const themeData = sessionStorage.getItem("themeData");
    // setUser(() => (userData ? JSON.parse(userData) : { token: null }));
    // setTheme(() =>
    //   themeData ? JSON.parse(themeData) : { mode: "dark", currentMenu: "home" }
    // );
  }, []);

  // const onMenuClick = (e) => {
  //   setTheme((oldValues) => {
  //     sessionStorage.setItem(
  //       "themeData",
  //       JSON.stringify({ ...oldValues, currentMenu: e.key })
  //     );
  //     return { ...oldValues, currentMenu: e.key };
  //   });
  // };

  // const handleLogout = (e) => {
  //   // e.stopPropagation(); // to stop event bubbling which will override the 'currentMenu' data
  //   setUser({ token: null });
  //   setSessionItem("userData", JSON.stringify({ token: null }));
  //   setSessionItem(
  //     "themeData",
  //     JSON.stringify({ currentMenu: "home", mode: "dark" })
  //   );
  //   location.reload(true);
  // };

  return (
    <UserContext.Provider
      value={{
          user,
          setUser,
        locationHook
        //   theme,
        //   setTheme,
        //   onMenuClick,
        //   messageApi,
        //   contextHolder,
        //   handleLogout,
        //   loadingState,
        //   setLoadingState,
      }}
    >
      {/* <AlertMessage /> */}
      {/* <ThemeProvider> */}
      <MainComponent />
      {/* </ThemeProvider> */}
    </UserContext.Provider>
  );
}

export default App;
