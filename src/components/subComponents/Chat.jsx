import { useState, useEffect, useRef } from "react";
import ShimmerUI from "../utilComponents/ShimmerUI";
import { getSessionItem } from "../../utils/helperFunctions/sessionStorageHelper";
import { publicServiceAPI } from "../../services/publicService";
import PopUpModal from "../utilComponents/PopUpModal";
import LoadingOverlay from "../utilComponents/LoadingSpinner";
import { PhotoProvider, PhotoView } from "react-photo-view";
import CustomAlert from "../utilComponents/CustomAlert";

import "react-photo-view/dist/react-photo-view.css";
const media = [
  {
    src: "https://oaidalleapiprodscus.blob.core.windows.net/private/org-BpErlF7oT8VaJ04F3rkWenEe/user-nt58Dch5McSi420CsbzYF1pl/img-Y2Fj6OyDnyB3gYecm2t1czj9.png?st=2024-07-24T09%3A29%3A04Z&se=2024-07-24T11%3A29%3A04Z&sp=r&sv=2023-11-03&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2024-07-23T23%3A17%3A05Z&ske=2024-07-24T23%3A17%3A05Z&sks=b&skv=2023-11-03&sig=yFoUeDsxnUFD/tve8y9LXG59jxffIjPivly9Dpxc9KU%3D",
  },
  {
    src: "https://cdn.pixabay.com/photo/2017/01/14/12/59/iceland-1979445_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2019/06/12/15/07/cat-4269479_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2016/12/04/21/58/rabbit-1882699_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2014/07/08/12/36/bird-386725_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2015/10/12/15/46/fallow-deer-984573_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2014/10/01/10/44/hedgehog-468228_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2013/09/22/15/29/prairie-dog-184974_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2018/03/31/06/31/dog-3277416_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313_960_720.jpg",
  },
  {
    src: "https://cdn.pixabay.com/photo/2019/03/09/17/30/horse-4044547_960_720.jpg",
  },
];

const ChatBot = () => {
  const [messageList, setMessageList] = useState([
    { user: "Support", message: "Hello there! Welcome to IDS-Support" },
    { user: "Support", message: "How may I help you?" },
  ]);
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState({ token: null });
  const [error, setError] = useState("");
  const [isError, setIsError] = useState({ status: false, message: "" });
  const [disableSend, setDisableSend] = useState(false);
  const [responseLoading, setResponseLoading] = useState(false);
  const chatEndRef = useRef(null);
  const srsEndRef = useRef(null);
  const [endChat, setEndChat] = useState(false);
  const [viewSRS, setViewSRS] = useState(false);
  const [srsDocument, setSRSDocument] = useState("");
  const [srsRawDocument, setSRSRawDocument] = useState("");
  const [viewMockUp, setViewMockUp] = useState(false);
  const [mockUpImages, setMockUpImages] = useState([]);
  const [isLoadingMockUpSRS, setIsLoadingMockUpSRS] = useState(false);
  const [showMockUp, setShowMockUp] = useState(false);

  const handleSendMessage = async (e) => {
    e.preventDefault();
    let tempMessage = message;
    setMessage("");
    if (message.length > 0) {
      insertToMessageList({ user: "Customer", message: message || "" });
      setDisableSend(true);
      setResponseLoading(true);
      try {
        const response = await publicServiceAPI("/send-message", {
          userId: user?.token,
          content: tempMessage,
        });
        console.log(response);
        if (response) {
          insertToMessageList({
            user: "Support",
            message: response?.externalApiResponse?.data?.content || "",
          });
          setDisableSend(false);
          setResponseLoading(false);
        }
      } catch (error) {
        // console.log(error);
        setError(error?.response?.data?.message || "Something went wrong");
        setDisableSend(false);
        setResponseLoading(false);
      }
    }
  };

  useEffect(() => {
    const fetchChatHistory = () => {
      const userData = getSessionItem("userData");
      if (userData) {
        setUser(() => (userData ? JSON.parse(userData) : { token: null }));
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      } else {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      }
    };

    fetchChatHistory();
  }, []);

  const insertToMessageList = (messageObject) => {
    setMessageList((oldValues) => {
      return [...oldValues, messageObject];
    });
    setTimeout(() => {
      scrollToBottom();
    }, 200);
  };

  const scrollToBottom = () => {
    chatEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  const scrollToSRSBottom = () => {
    srsEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };
  function convertArrayToText(messageList) {
    return messageList
      ?.map((item) => `${item.user}: ${item.message}`)
      .join(" ");
  }

  const submitChat = async () => {
    try {
      setResponseLoading(true);
      setDisableSend(true);
      const response = await publicServiceAPI("/summarize-conversation", {
        userId: user?.token,
        conversation: convertArrayToText(messageList) || "",
      });
      console.log(response);
      if (response) {
        insertToMessageList({
          user: "Support",
          message: "Thank you for contacting IDS-Support." || "",
        });
        insertToMessageList({
          user: "Support",
          message:
            "For any queries, please drop an mail to sales@Indeadesignsystems.com." ||
            "",
        });
        insertToMessageList({
          user: "Support",
          message: "Have a great day!" || "",
        });
        insertToMessageList({
          user: "Support",
          message: "Click below link to view the SRS",
          srsLink: "generate-srs",
        });
        // insertToMessageList({
        //   user: "Support",
        //   message: "Click below link to view the Mock-Up's",
        //   mockupLink: "generate-mockup-images",
        // });

        // setDisableSend(false);
        setResponseLoading(false);
        setEndChat(true);
      }
    } catch (error) {
      setError(error?.response?.data?.message || "Something went wrong");
      setResponseLoading(false);
      setDisableSend(false);
    }
  };

  // SRS View
  const handleViewSRSClick = async () => {
    try {
      setIsLoadingMockUpSRS(true);
      const response = await publicServiceAPI("/generate-srs", {
        userId: user?.token,
        conversation: convertArrayToText(messageList) || "",
      });
      console.log(response);
      if (response.data) {
        // TODO: Open pop-up modal and display SRS Document
        // console.log(parseSRS(response?.data || ""));
        setSRSDocument(parseSRS(response?.data) || "");
        setSRSRawDocument(response?.data || "");
        setViewSRS(true);
        setIsLoadingMockUpSRS(false);
      }

      // Testing
      // setSRSDocument("");
      // setViewSRS(true);
      // setIsLoadingMockUpSRS(false);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingMockUpSRS(false);
  };

  // Mock-Up View
  const handleViewMockUpClick = async () => {
    try {
      setIsLoadingMockUpSRS(true);
      const response = await publicServiceAPI("/generate-image", {
        userId: user?.token,
        srs: srsRawDocument || "",
      });
      console.log(response);
      if (response) {
        // TODO: Open pop-up modal and display SRS Document
        setViewMockUp(true);
        setViewSRS(false);
        response?.map((item) => {
          return {
            src: item?.mockupLink || "",
          };
        });
        setMockUpImages(
          response?.map((item, index) => {
            return {
              ...item,
              src: item?.mockupLink || "",
              key: item.name + index,
            };
          }) || []
        );
        setIsLoadingMockUpSRS(false);
        setViewMockUp(true);
        setShowMockUp(true);
        setTimeout(() => {
          scrollToSRSBottom();
        }, 50000);
      }
    } catch (error) {
      console.log(error);
      setIsError({
        status: true,
        message: "Unable to get the MockUp at the moment",
      });
      setTimeout(() => {
        setIsError({
          status: false,
          message: "",
        });
      }, 50000);
    }
    setIsLoadingMockUpSRS(false);
  };

  // Format SRS Document
  const parseSRS = (srsText) => {
    const lines = srsText.split("\n");
    console.log("lines : ", lines);
    const docContent = [];
    let isHeading = false; // Track if the current line is a heading
    lines.forEach((line, index) => {
      if (line.startsWith("## ")) {
        docContent.push(
          <h1
            className="text-lg font-semibold"
            key={index}
            style={{ marginBottom: "20px" }}
          >
            {line.replace("## ", "")}
          </h1>
        );
        isHeading = true;
      } else if (line.startsWith("**")) {
        const parts = line.split("**");
        docContent.push(
          <p className="mt-2" key={index} style={{ marginBottom: "10px" }}>
            {parts.map((part, index) =>
              index % 2 === 0 ? part : <strong key={index}>{part}</strong>
            )}
          </p>
        );
        isHeading = false;
      } else if (line.startsWith("* ")) {
        const lineData = line.replace("* ", "");
        const parts = lineData.split("**");

        docContent.push(
          <ul className="list-disc list-outside ml-7 mt-2">
            <li>
              <p
                className="mt-2"
                key={index + 1}
                style={{ marginBottom: "10px" }}
              >
                {parseSRS(lineData)}
                {/* {parts.map((part, index) =>
                  index % 2 === 0 ? (
                    part
                  ) : (
                    <strong key={index + 1}>{parseSRS(part)}</strong>
                  )
                )} */}
              </p>
            </li>
          </ul>
        );
        isHeading = false;
      } else if (line.startsWith("    * ")) {
        const lineData = line.replace("* ", "");
        const parts = lineData.split("**");
        const liItem = parts.map((part, index) =>
          index % 2 === 0 ? part : <strong key={index}>{part}</strong>
        );
        docContent.push(
          <ul
            className="list-disc list-inside ml-5"
            key={index}
            style={{ marginBottom: "10px", marginLeft: "20px" }}
          >
            <li>{line.replace("    * ", "")}</li>
          </ul>
        );
        isHeading = false;
      } else if (line.startsWith("        * ")) {
        const lineData = line.replace("* ", "");
        const parts = lineData.split("**");
        const liItem = parts.map((part, index) =>
          index % 2 === 0 ? part : <strong key={index}>{part}</strong>
        );
        docContent.push(
          <ul
            className="list-disc list-inside ml-5"
            key={index}
            style={{ marginBottom: "10px", marginLeft: "30px" }}
          >
            <li>{line.replace("        * ", "")}</li>
          </ul>
        );
        isHeading = false;
      } else {
        if (index > 0 && isHeading) {
          // Add spacing only if previous line was a heading
          docContent.push(
            <p key={index} style={{ marginBottom: isHeading ? "10px" : "0" }}>
              {line}
            </p>
          );
        } else {
          docContent.push(
            <p key={index} style={{ marginBottom: isHeading ? "10px" : "0" }}>
              {line}
            </p>
          );
        }
        isHeading = false;
      }
    });
    return docContent;
  };

  const handlePopUpCloseClick = () => {
    setSRSDocument("");
    setSRSRawDocument("");
    setMockUpImages([]);
    setViewSRS(false);
  };

  const handleMockUpPopUpCloseClick = () => {
    setViewMockUp(false);
    setViewSRS(true);
  };

  if (isLoading) {
    return <ShimmerUI />;
  }

  if (user) {
    return (
      <div className="bg-transparent flex p-5 flex-col items-center gap-5 bg-slate-50">
        {isLoadingMockUpSRS && <LoadingOverlay />}
        {isError.status && <CustomAlert message={isError.message} />}
        <div
          className="shadow-slate-400 shadow-3xl w-full max-w-md bg-white rounded-lg overflow-hidden"
          style={{
            minWidth: "550px",
          }}
        >
          <div className="flex flex-col  justify-between">
            <div className="bg-green-600 text-white p-4">
              <h1 className="text-lg font-semibold text-slate-700">
                Welcome!{" "}
                <span className="text-white">{user?.userDetails?.name}</span>
              </h1>
            </div>
            <div className="h-auto">
              <div
                id="chat-messages"
                className="flex flex-col justify-between min-h-96 p-4 pb-0 space-y-4 overflow-y-auto max-auto h-full max-h-96 scrollbar-hidden mb-4"
                style={{
                  minHeight: "500px",
                }}
              >
                <div className="space-y-4">
                  {messageList.map((message, index) => (
                    <div
                      key={message.user + index}
                      className={`flex items-start whitespace-pre-wrap ${
                        message.user != "Support" ? "justify-end" : ""
                      }`}
                    >
                      <div
                        className={`${
                          message.user != "Support"
                            ? "bg-green-600 text-white"
                            : "bg-gray-200 text-gray-800"
                        } p-3 rounded-lg ${
                          message.user != "Support"
                            ? "rounded-br-none"
                            : "rounded-bl-none"
                        }`}
                      >
                        <p>{message.message}</p>
                        {message?.srsLink && (
                          <button
                            type="button"
                            onClick={handleViewSRSClick}
                            className="mt-2 w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-2 py-1 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  sm:w-auto sm:text-sm hover:cursor-pointer"
                          >
                            View SRS
                          </button>
                        )}
                        {/* {message?.mockupLink && (
                          <span
                            onClick={handleViewMockUpClick}
                            className="text-blue-500 hover:text-blue-900 hover:cursor-pointer"
                          >
                            View Mock-Up's
                          </span>
                        )} */}
                      </div>
                    </div>
                  ))}
                  {responseLoading && (
                    <div className="w-fit flex items-start loading-dots bg-gray-200 text-gray-800 rounded-bl-none p-3 rounded-lg">
                      <span>.</span>
                      <span>.</span>
                      <span>.</span>
                    </div>
                  )}
                </div>
                {messageList?.length > 6 && !endChat && (
                  <div className="flex items-center flex-col">
                    {error && <span className="text-red-700">{error}</span>}
                    <button
                      className="rounded-lg p-2 bg-red-300 w-fit"
                      onClick={submitChat}
                    >
                      Press to End Chat
                    </button>
                  </div>
                )}
                <div ref={chatEndRef}></div>
              </div>
              {/* Input Area */}
              {!endChat && (
                <div className="border-t border-gray-200 p-4 mt-2">
                  <form className="flex space-x-2" onSubmit={handleSendMessage}>
                    <input
                      type="text"
                      value={message}
                      onChange={(e) => setMessage(e.target.value)}
                      className="w-full p-2 border rounded-lg focus:outline-none focus:border-green-600"
                      placeholder="Type a message..."
                    />
                    <button
                      type="submit"
                      className="bg-green-600 text-white px-4 py-2 rounded-lg hover:bg-green-700"
                      disabled={disableSend}
                    >
                      Send
                    </button>
                  </form>
                </div>
              )}
            </div>
          </div>
        </div>
        {/* <Modal setShowModal={setViewSRS} showModal={viewSRS} /> */}
        <PopUpModal
          handlePopUpCloseClick={handlePopUpCloseClick}
          showModal={viewSRS}
          handleViewMockUpClick={handleViewMockUpClick}
          showMockUp={showMockUp}
          mockupImages={mockUpImages}
          title={"View SRS"}
          footerBottonTitle={"View Mock-Up"}
          bgColor={"blue"}
        >
          <div className="mt-4 h-[600px] overflow-y-auto">
            <div className="text-sm text-gray-500">{srsDocument}</div>
          </div>
        </PopUpModal>
        <PopUpModal
          setShowModal={setViewMockUp}
          showModal={viewMockUp}
          handleViewMockUpClick={handleMockUpPopUpCloseClick}
          handlePopUpCloseClick={handleMockUpPopUpCloseClick}
          title={"View MockUp"}
          footerBottonTitle={"Close"}
          bgColor={"blue"}
        >
          <div className="mt-4 max-h-[600px] overflow-y-auto">
            <div className="mt-5 overflow-hidden rounded-lg border border-gray-200">
              <table className="min-w-full">
                <thead>
                  <tr>
                    <th className="py-2 px-4 border-b">Name</th>
                    <th className="py-2 px-4 border-b">Description</th>
                    <th className="py-2 px-4 border-b">Image</th>
                  </tr>
                </thead>
                <tbody>
                  {mockUpImages.map((item) => (
                    <tr key={item.key}>
                      <td className="py-2 px-4 border-b">{item.name}</td>
                      <td className="py-2 px-4 border-b">
                        <ul className="list-disc">
                          {item.description?.fields?.map((item, liIndex) => {
                            if (typeof item == "string") {
                              return <li key={item + liIndex}>{item}</li>;
                            }
                          })}
                          {item.description?.features?.map((item, liIndex) => {
                            if (typeof item == "string") {
                              return <li key={item + liIndex}>{item}</li>;
                            }
                          })}
                        </ul>
                      </td>
                      <td className="py-2 px-4 border-b">
                        {item.mockupLink != "" ? (
                          <PhotoProvider>
                            <PhotoView key={item.name} src={item.mockupLink}>
                              <img
                                src={item.mockupLink}
                                alt=""
                                className="max-w-32 cursor-pointer"
                              />
                            </PhotoView>
                          </PhotoProvider>
                        ) : (
                          <span className="text-red-500">
                            Available in Pro-Version
                          </span>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </PopUpModal>
      </div>
    );
  }
};

export default ChatBot;
