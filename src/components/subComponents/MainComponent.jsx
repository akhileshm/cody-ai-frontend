import React, { useEffect, useState } from "react";
import { useUserContext } from "../../utils/contextProviders/UserContext";
import { Navigate, Outlet } from "react-router-dom";
// import { Flex, Layout, Spin } from "antd";
// import HeaderComponent from "../../layout/Header";
// import FooterComponent from "../../layout/Footer";
// import { contentStyle, footerStyle, headerStyle, layoutStyle } from "./styles";
import { getSessionItem } from "../../utils/helperFunctions/sessionStorageHelper";
// import LoadingState from "../../utilComponents/LoadingState";
import ShimmerUI from "../utilComponents/ShimmerUI";
// const { Header, Content, Footer } = Layout;
const MainComponent = () => {
  const { locationHook } = useUserContext();
  const [user, setUser] = useState({ token: null });
  const [isLoading, setIsLoading] = useState(true);

  // const [theme, setTheme] = useState({ mode: "dark", currentMenu: "" });
  useEffect(() => {
    const fetchUserData = () => {
      const userData = getSessionItem("userData");
      if (userData) {
        setUser(() => (userData ? JSON.parse(userData) : { token: null }));
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      } else {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      }
    };
    // sessionStorage.setItem("userData", JSON.stringify(user));
    // const themeData = getSessionItem("themeData");
    // setTheme(() =>
    //   themeData ? JSON.parse(themeData) : { mode: "dark", currentMenu: "home" }
    // );
    fetchUserData();
  }, []);

  if (isLoading) {
    // Display loading indicator or message while user data is being loaded
    return (
      // <div className="h-screen">
      //   <Spin spinning={isLoading} className="h-screen !max-h-none">
      //     <div className="h-screen">Loading...</div>
      //   </Spin>
      // </div>
      <ShimmerUI />
    );
  }

  if (user) {
    return user?.token ? (
      locationHook.pathname === "/login" ? (
        <Navigate to="/" />
      ) : (
        //   <UserContext.Provider value={{ user, locationHook, theme }}>
        // <Flex gap="middle" wrap>
        <div className="app main-section relative font-nunito text-sm font-normal antialiased">
          {/* <Layout style={layoutStyle}> */}
          {/* <Header style={headerStyle} className="z-20"> */}
          {/* <HeaderComponent /> */}
          {/* </Header> */}
          {/* <div className="bg-blue-600 text-white p-4">
            <h1 className="text-xl">Chat App</h1>
          </div>
          <hr
            className="z-10 sticky"
            style={{
              boxShadow: "rgba(0, 0, 0, 0.37) 0px -4px 9px 3px",
            }}
          /> */}
          {/* <Content style={contentStyle}> */}
          <div className="bg-black h-screen flex flex-col  justify-center" style={{
        background: "radial-gradient(circle, rgba(83,83,83,1) 0%, rgba(0,0,0,1) 100%)"
      }}>
            <Outlet />
          </div>
          {/* </Content> */}
          {/* <Footer style={footerStyle}> */}
          {/* <FooterComponent /> */}
          {/* <div>Footer Component</div> */}
          {/* </Footer> */}
          {/* </Layout> */}
        </div>
        // <LoadingState />
        // </Flex>
        //   </UserContext.Provider>
      )
    ) : locationHook?.pathname !== "/login" ? (
      <Navigate to="/login" />
    ) : (
      <div className="main-section relative font-nunito text-sm font-normal antialiased">
        <Outlet />
      </div>
    );
  }
};

export default MainComponent;
