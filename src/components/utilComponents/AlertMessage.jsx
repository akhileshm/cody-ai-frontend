import React from "react";
import { useUserContext } from "../../utils/contextProviders/UserContext";

const AlertMessage = () => {
  const { contextHolder } = useUserContext();
  //   NOTE: Do not remove commented code, this below code is a template
  //   const handleAlert = () => {
  //   Example for triggering an alert
  //     messageApi.open({
  //       type: alert?.type || "info",
  //       content: alert?.content || "Alert",
  //     });
  //   };

  return <>{contextHolder}</>;
};

export default AlertMessage;
