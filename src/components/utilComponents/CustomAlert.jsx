import { useState, useEffect } from "react";

const Alert = ({ message }) => {
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
    }, 50000);

    return () => clearTimeout(timer);
  }, []);

  return (
    visible && (
      <div className="fixed flex top-4 z-50 left-1/2 transform -translate-x-1/2 bg-red-500 text-white px-4 py-2 rounded shadow-lg transition-opacity duration-1000 ease-in-out">
        <svg
          class="w-6 h-6 text-white"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M8.257 3.099c.765-1.36 2.721-1.36 3.486 0l6.518 11.618c.75 1.338-.213 3-1.742 3H3.481c-1.53 0-2.492-1.662-1.742-3L8.257 3.1zM11 13a1 1 0 10-2 0 1 1 0 002 0zm-1-3a1 1 0 00-.993.883L9 11v2a1 1 0 001.993.117L11 13v-2a1 1 0 00-.883-.993L10 10z"
            clipRule="evenodd"
          />
        </svg>
        {message}
      </div>
    )
  );
};

export default Alert;
