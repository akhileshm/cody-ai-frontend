import React from "react";
import LoaderImage from "../../assets/images/loading-img.gif";

const LoadingOverlay = () => {
  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-50 backdrop-filter backdrop-blur-sm">
      <img src={LoaderImage} alt="loader" />
    </div>
  );
};

export default LoadingOverlay;
