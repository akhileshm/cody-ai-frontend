import React from "react";
import { Modal, Spin } from "antd";
import { useUserContext } from "../../utils/contextProviders/UserContext";
const LoadingState = () => {
  const { loadingState } = useUserContext();
  return (
    <Modal
      open={loadingState}
      modalRender={() => (
        <div className="flex h-full w-full">
          <Spin spinning={loadingState} style={{ width: "100%" }}></Spin>
        </div>
      )}
      maskClosable={false}
      wrapClassName="bg-white/5"
      centered
      footer={false}
      closable={false}
    />
  );
};

export default LoadingState;
