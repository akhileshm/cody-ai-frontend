const Modal = ({
  showModal,
  handlePopUpCloseClick,
  children,
  handleViewMockUpClick,
  showMockUp,
  mockupImages,
  title,
  footerBottonTitle,
  bgColor,
}) => {
  return (
    <>
      {showModal ? (
        <div className="fixed inset-0 z-40 overflow-y-auto">
          <div className="flex items-center justify-center min-h-screen px-4 text-center sm:block sm:p-0">
            <div
              className="fixed inset-0 transition-opacity"
              aria-hidden="true"
              // onClick={() => setShowModal(false)}
            >
              <div className="absolute inset-0 bg-gray-800 opacity-75"></div>
            </div>

            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>

            <div className="inline-block bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:align-middle sm:max-w-[900px] sm:w-full">
              <div className="bg-white p-6 pb-3">
                <div className="text-right"></div>
                <div className="flex flex-row justify-between">
                  <div className="">
                    <h3 className="text-lg leading-6 font-medium text-gray-900">
                      {title}
                    </h3>
                  </div>
                  <div className="">
                    <button
                      type="button"
                      className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center"
                      onClick={handlePopUpCloseClick}
                    >
                      <svg
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 011.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
                {children}
              </div>
              <div className="bg-gray-50 px-4 pb-3 flex justify-center">
                <button
                  type="button"
                  className={`w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 text-base font-medium text-white bg-${bgColor}-500 hover:bg-${bgColor}-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-${bgColor}-500 sm:ml-3 sm:w-auto sm:text-sm`}
                  onClick={handleViewMockUpClick}
                >
                  {footerBottonTitle}
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Modal;
