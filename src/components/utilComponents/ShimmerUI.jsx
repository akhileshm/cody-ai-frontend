import React from "react";

const ShimmerUI = () => {
  return (
    <div className="h-screen flex gap-3 flex-col w-full">
      <div className="h-14 p-5 bg-gray-300 rounded shimmer flex flex-row content-between">
        <div className="h-full flex flex-row w-full  items-center gap-5">
          <div className="h-10  w-72 rounded shimmer-dark ring-1 ring-blue-50"></div>
          <div className="h-10  w-20 rounded shimmer-dark ring-1 ring-blue-50"></div>
        </div>
        <div className="flex justify-center items-center">
          <div className="h-10  w-10 rounded shimmer-dark ring-1 ring-blue-50"></div>
        </div>
      </div>
      <div className="h-16 bg-gray-300 rounded shimmer mx-5">
        <div className="h-full flex flex-row w-full justify-center items-center gap-5">
          <div className="h-8  w-72 rounded shimmer-dark ring-1 ring-blue-50"></div>
          <div className="h-8  w-72 rounded shimmer-dark ring-1 ring-blue-50"></div>
          <div className="h-8  w-72 rounded shimmer-dark ring-1 ring-blue-50"></div>
          <div className="h-8  w-72 rounded shimmer-dark ring-1 ring-blue-50"></div>
          <div className="h-8  w-20 rounded shimmer-dark ring-1 ring-blue-50"></div>
        </div>
      </div>
      <div className="h-1/2 rounded shimmer mx-5 p-6">
        <div className="flex flex-col w-full h-full gap-3">
          <div className="h-8  w-72 rounded shimmer-dark"></div>
          <div className="w-full h-full rounded shimmer-dark"></div>
        </div>
      </div>
    </div>
  );
};

export default ShimmerUI;
