import React from "react";
import ChatBot from "../components/subComponents/Chat";

const Dashboard = () => {
  return (
    <div className="w-auto h-auto bg-transparent">
      <ChatBot />
    </div>
  );
};

export default Dashboard;
