import { useState } from "react";
import { loginServiceAPI, otpServiceAPI } from "../services/publicService";
import { useNavigate } from "react-router-dom";
import { useUserContext } from "../utils/contextProviders/UserContext";
import { setSessionItem } from "../utils/helperFunctions/sessionStorageHelper";
import LoginImg from "../assets/images/ai-img-6.gif";
import LoadingOverlay from "../components/utilComponents/LoadingSpinner";

const Login = () => {
  const [userDetails, setUserDetails] = useState({
    name: "",
    email: "",
    phone: "",
  });
  const [consent, setConsent] = useState(false);
  const [error, setError] = useState(null);
  const [page, setPage] = useState("login");
  const [otp, setOtp] = useState("");
  const [industry, setIndustry] = useState("");
  const [userId, setUserId] = useState("");
  const [disableSubmit, setDisableSubmit] = useState(false);
  const [disableVerify, setDisableVerify] = useState(false);
  const [disableIndustry, setDisableIndustry] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();
  const { locationHook, user, setUser } = useUserContext();

  const handleLoginSubmit = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      setDisableSubmit(true);
      setIsLoading(true);
      // TODO: Api call
      try {
        const response = await loginServiceAPI("/create-user", userDetails);
        console.log(response);
        if (response) {
          setPage("otp");
          setUserId(response?.id);
          setDisableSubmit(false);
          setIsLoading(false);
        }
      } catch (error) {
        console.log(error);
        let stringArray = error?.response?.data?.error?.split(".");
        console.log(stringArray)
        setError(
          stringArray[0]?.replace(/'To'/g, "").trim() ||
            "Something went wrong"
        );
        setDisableSubmit(false);
        setIsLoading(false);
      }
    } else {
      setError("You need to provide consent to proceed.");
    }
  };

  const validateForm = () => {
    setError(null);
    return !!consent;
  };

  const handleOtpSubmit = async (e) => {
    e.preventDefault();
    if (otp != "") {
      setDisableVerify(true);
      setIsLoading(true);
      try {
        setError("");
        const response = await otpServiceAPI("/verify-otp", {
          otp,
          userId,
        });
        console.log(response);
        if (response) {
          //   Navigate to dashboard page
          // navigate("/dashboard");
          setSessionItem(
            "userData",
            JSON.stringify({
              token: response?.user?.id || null,
              userDetails: response?.user || {},
            })
          );
          setDisableVerify(true);
          setPage("industry");
          setIsLoading(false);
        }
      } catch (error) {
        // console.log(error);
        setError(error?.response?.data?.message || "Something went wrong");
        setDisableVerify(false);
        setIsLoading(false);
      }
    }
  };

  const handleIndustrySubmit = async (e) => {
    e.preventDefault();
    if (industry != "") {
      setDisableIndustry(true);
      setIsLoading(true);
      try {
        const response = await otpServiceAPI("/update-industry", {
          userId: userId,
          industry: industry,
        });
        console.log(response);
        if (response) {
          //   Navigate to dashboard page
          // navigate("/dashboard");
          reloadPage();
          setDisableIndustry(true);
        }
      } catch (error) {
        // console.log(error);
        setError(error?.response?.data?.message || "Something went wrong");
        setDisableIndustry(false);
        setIsLoading(false);
      }
    }
  };

  const handleInputChange = (e) => {
    setUserDetails((oldValues) => {
      return {
        ...oldValues,
        [e.target.name]: e.target.value,
      };
    });
  };

  const handleConsentChange = (e) => {
    setConsent(e.target.checked);
  };

  const reloadPage = () => {
    setTimeout(() => {
      setIsLoading(false);
      location.reload(true);
    }, 2000);
  };

  return (
    <div
      className="w-full text-white h-screen flex flex-col text-center items-center justify-center bg-black"
      style={{
        background:
          "radial-gradient(circle, rgba(83,83,83,1) 0%, rgba(0,0,0,1) 100%)",
      }}
    >
      {isLoading && <LoadingOverlay />}
      <div className="p-5  justify-center flex">
        <img src={LoginImg} alt="img" />
      </div>
      {page == "login" && (
        <div className="w-1/2">
          <h1 className="text-5xl font-extrabold">Marketing Assistance Chat</h1>
          <div className="flex items-start border border-gray-50 rounded-md p-8 m-8">
            <form onSubmit={handleLoginSubmit} className="text-left w-full">
              <div className="p-2 m-2 flex items-center">
                <label className="w-40">Email:</label>
                <input
                  type="email"
                  name="email"
                  value={userDetails.email}
                  onChange={handleInputChange}
                  required
                  className="flex-1 border border-gray-300 p-2 rounded text-black"
                />
              </div>
              <div className="p-2 m-2 flex items-center">
                <label className="w-40">Name:</label>
                <input
                  type="text"
                  name="name"
                  value={userDetails.name}
                  onChange={handleInputChange}
                  required
                  className="flex-1 border border-gray-300 p-2 rounded text-black"
                />
              </div>
              <div className="p-2 m-2 flex items-center">
                <label className="w-40">Phone:</label>
                <input
                  type="text"
                  name="phone"
                  value={userDetails.phone}
                  onChange={handleInputChange}
                  className="flex-1 border border-gray-300 p-2 rounded text-black"
                  placeholder="Ex: +1-212-456-7890, +919876543210"
                />
              </div>
              <div className="p-2 m-2 flex items-center">
                <input
                  id="consent"
                  type="checkbox"
                  checked={consent}
                  onChange={handleConsentChange}
                  className="mr-2 transform scale-150"
                />{" "}
                <label htmlFor="consent">
                  I consent to using this information for marketing assistance.
                </label>
              </div>
              {error && <div style={{ color: "red" }}>{error}</div>}
              <div className="p-2 m-2 flex items-center justify-center">
                <button
                  type="submit"
                  className="ml-2 p-2 bg-green-500 text-white rounded"
                  disabled={disableSubmit}
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
      {page == "otp" && (
        <>
          <h1 className="text-5xl font-extrabold">OTP</h1>
          <div className="flex items-start border border-gray-50 rounded-md p-8 m-8">
            <form onSubmit={handleOtpSubmit} className="text-left w-full">
              <div className="p-2 m-2 flex items-center">
                <label className="w-40" htmlFor="otp">
                  OTP:
                </label>
                <input
                  type="otp"
                  name="otp"
                  value={otp}
                  onChange={(e) => setOtp(e.target.value)}
                  required
                  className="flex-1 border border-gray-300 p-2 rounded text-black"
                />
              </div>
              <div className="p-2 m-2 flex items-center justify-center">
                <button
                  type="submit"
                  className="ml-2 p-2 bg-green-500 text-white rounded"
                  disabled={disableVerify}
                >
                  Verify
                </button>
              </div>
            </form>
          </div>
          {error && <div style={{ color: "red" }}>{error}</div>}
        </>
      )}
      {page == "industry" && (
        <>
          <h1 className="text-5xl font-extrabold">Mention your Industry</h1>
          <div className="flex items-start border border-gray-50 rounded-md p-8 m-8">
            <form onSubmit={handleIndustrySubmit} className="text-left w-full">
              <div className="p-2 m-2 flex items-center">
                <label className="w-40" htmlFor="industry">
                  Industry
                </label>
                <input
                  id="industry"
                  type="industry"
                  name="industry"
                  value={industry}
                  onChange={(e) => setIndustry(e.target.value)}
                  required
                  className="flex-1 border border-gray-300 p-2 rounded text-black"
                />
              </div>
              <div className="p-2 m-2 flex items-center justify-center">
                <button
                  type="button"
                  className="ml-2 p-2 bg-blue-500 text-white rounded"
                  onClick={reloadPage}
                >
                  Skip
                </button>
                <button
                  type="submit"
                  className="ml-2 p-2 bg-green-500 text-white rounded"
                  disabled={disableIndustry}
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
          {error && <div style={{ color: "red" }}>{error}</div>}
        </>
      )}
    </div>
  );
};

export default Login;
