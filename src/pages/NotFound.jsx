import React from "react";
// import bgLogo from "../../assets/images/bg-not-found5.png";
// import notFoundLogo from "../../assets/images/not-found2.png";

const NotFound = () => {
  return (
    <div
      style={{
        // backgroundImage: `url(${bgLogo})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
      }}
      className="flex items-center w-full"
    >
      <div
        style={{
          // backgroundImage: `url(${notFoundLogo})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          width: "500px",
          height: "300px",
        }}
        className="flex items-center"
      >
        Page Not Found
      </div>
    </div>
  );
};

export default NotFound;
