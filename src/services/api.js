import { securedAxiosInstance, axiosInstance } from "./apiConfig";

export const apiService = async (method, url, data = null, config = {}) => {
  try {
    let response;
    switch (method.toLowerCase()) {
      case "get":
        response = await securedAxiosInstance.get(url, config);
        break;
      case "post":
        response = await securedAxiosInstance.post(url, data, config);
        break;
      case "put":
        response = await securedAxiosInstance.put(url, data, config);
        break;
      case "delete":
        response = await securedAxiosInstance.delete(url, config);
        break;
      default:
        throw new Error(`Unsupported method: ${method}`);
    }
    return response.data;
  } catch (error) {
    // handleApiError(error);
    throw error;
  }
};

export const publicApiService = async (url, data = null) => {
  try {
    let response;
    response = await axiosInstance.post(url, data);
    return response.data;
  } catch (error) {
    throw error;
  }
};
// Error handling function
const handleApiError = (error) => {
  console.error("API call message: ", error.message);
  console.error("API call failed: ", error);
  // Further error handling logic like showing notifications can be added here
};
