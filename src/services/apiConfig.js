import axios from "axios";

const userObject = sessionStorage.getItem("userData");
const userData = userObject ? JSON.parse(userObject) : { token: null };

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_BASE_API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

const securedAxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_BASE_API_URL,
  headers: {
    "Content-Type": "application/json",
    Authorization: userData?.token,
  },
});

export { axiosInstance, securedAxiosInstance };
