import { apiService } from "./api";

const userDataDetails = sessionStorage.getItem("userData");
const userData = userDataDetails
  ? JSON.parse(typeof userDataDetails == 'object')
  : { token: null };
const headers = { Authorization: userData.token };

// Example function to get data
export const getServiceAPI = async (url) => {
  return await apiService("get", url, null, headers);
};

// Example function to post data
export const postServiceAPI = async (url, data) => {
  return await apiService("post", url, data, headers);
};

// Example function to update data
export const updateServiceAPI = async (url, data, id) => {
  return await apiService("post", `${url}${id}`, data, headers);
};

// Example function to delete data
export const deleteServiceAPI = async (url, id) => {
  return await apiService("delete", `${url}${id}`, null, headers);
};
