import { publicApiService } from "./api";

// Example function to post data
export const loginServiceAPI = async (url, data) => {
  return await publicApiService(url, data);
};

export const otpServiceAPI = async (url, data) => {
  return await publicApiService(url, data);
};

export const publicServiceAPI = async (url, data) => {
  return await publicApiService(url, data);
};
