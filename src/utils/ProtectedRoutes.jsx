import { Navigate, Outlet } from "react-router-dom"
import Headers from '../components/layout/Header'
import Sidebar from '../components/layout/Sidebar'
import Footer from '../components/layout/Footer'

const ProtectedRoutes = () => {
    let auth = { token: null }
    return (auth.token ? <>
        <Headers />
        <Sidebar>
            <Outlet />
        </Sidebar>
        <Footer />
    </>
        : <Navigate to="/login" />)
}

export default ProtectedRoutes