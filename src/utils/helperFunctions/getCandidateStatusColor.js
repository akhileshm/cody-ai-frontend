export const getStatusColor = (value) => {
  switch (value) {
    case "New":
      return "blue";
    case "On-Hold":
      return "orange";
    case "Converted - Employee":
      return "pink";
    case "Blocked":
      return "danger";
    default:
      return "geekblue";
  }
};
